package com.patterns.examples.state;

interface State {
    void openClose();
}

