package com.patterns.examples.state;

public class Close implements State {
    @Override
    public void openClose() {
        System.out.println("StatePattern.com.patterns.state.Door is close.");
    }
}